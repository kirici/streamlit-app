"""
Commands for browsing a website
"""

import json
from time import sleep

import requests
from selenium import webdriver
# from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By


def init_options() -> webdriver.ChromeOptions:
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-ssl-errors=yes')
    options.add_argument('--ignore-certificate-errors')
    return options


# def clear_sessions(session_id=None):
#     """
#     Here we query and delete orphan sessions
#     docs: https://www.selenium.dev/documentation/grid/advanced_features/endpoints/
#     :return: None
#     """
#     url = "http://127.0.0.1:4444"
#     if not session_id:
#         # delete all sessions
#         r = requests.get("{}/status".format(url))
#         data = json.loads(r.text)
#         for node in data['value']['nodes']:
#             for slot in node['slots']:
#                 if slot['session']:
#                     id = slot['session']['sessionId']
#                     r = requests.delete("{}/session/{}".format(url, id))
#     else:
#         # delete session from params
#         r = requests.delete("{}/session/{}".format(url, session_id))

def readiness_probe(wd_url: str):
    ready = False
    while not ready:
        try:
            response = requests.get("{}/status".format(wd_url))
        except requests.exceptions.RequestException as err:
            print(f"Selenium grid is not ready: {err}")
            sleep(1)
        else:
            json_payload = json.loads(response.text)
            if json_payload['value']['ready'] is True:
                ready = True


def drive(grid_url: str, target_app_url: str):
    driver = webdriver.Remote(
        command_executor='http://grid:4444/wd/hub',
        options=init_options()
    )

    driver.maximize_window()
    driver.get(target_app_url + "/Orders")
    sleep(1)
    button_text = "Espresso"
    buttons = driver.find_elements(By.XPATH, "//button")
    for button in buttons:
        if button.text == button_text:
            button.click()
            # TODO: structured logging
            print("CLICKED " + str(button))
            break
    sleep(3)
    driver.get(target_app_url + "/")
    sleep(1)
    button_text = "get"
    buttons = driver.find_elements(By.XPATH, "//button")
    for button in buttons:
        if button.text == button_text:
            button.click()
            print("CLICKED " + str(button))
            break
    driver.close()
    driver.quit()


def main():
    # clear_sessions()
    # TODO: get from env vars
    grid_url: str = "http://grid:4444"
    target_app_url = "http://streamlit-app:8501"
    readiness_probe(grid_url)
    drive(grid_url, target_app_url)


main()
