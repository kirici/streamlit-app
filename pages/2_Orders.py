import time

import streamlit as st

st.set_page_config(
    page_title="Sample",
    page_icon="🔥",
    layout="wide",
    initial_sidebar_state="expanded",
)


def coffee_order(drink: str):
    latest_iteration = st.empty()
    pbar = st.progress(0)
    for i in range(100):
        # ml per percent for an espresso
        c = (i + 1) / 4
        latest_iteration.text(f"{int(c)} ml coffee extracted")
        pbar.progress(i + 1)
        time.sleep(0.02)
    st.success("One " + drink + " ready to go!")
    st.session_state.orders[drink] = ""
    st.write(st.session_state.orders[drink])


def restart_machine_button():
    if "start_reset" in st.session_state and st.session_state.start_reset is True:
        st.session_state.running = True
    else:
        st.session_state.running = False

    if st.button("Machine reset", disabled=st.session_state.running, key="start_reset"):
        st.toast("Restarting coffee machine...")
        status = st.progress(0)
        t = 4
        for i in range(100):
            status.progress(i)
            time.sleep(t / 100)

        st.success("Coffee machine ready!")
        status.empty()

    if st.session_state.start_reset is True:
        with st.spinner(text="Reset cooldown"):
            time.sleep(15)
        st.session_state.running = False
        st.rerun()


def init_state():
    if "orders" not in st.session_state:
        st.session_state.orders = {}


def page_dressing():
    st.title("Coffee shop")
    st.markdown("#### Place your orders here")
    st.sidebar.markdown("# Orders")


init_state()
page_dressing()
if st.button("Espresso", type="primary"):
    coffee_order(drink="Espresso")
restart_machine_button()
