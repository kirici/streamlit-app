# Sample streamlit app

### Purpose

Basic and barebones placeholder application to be used to test testing.

### Notes

It should not be noticeable, but git is not in use at all except as storage backend.
See [jj](https://github.com/martinvonz/jj) for more information.

##### Linters, formatters

isort, pycodestyle, pyflakes, ruff - all defaults except 120 char line length.
