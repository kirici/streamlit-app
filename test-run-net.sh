#!/bin/bash
set -eu

# Global consts
CTR_PORT=9090

# Global vars
SCRIPT_PATH="$(dirname "$(realpath "$0")")"
PROJECT_NAME="${SCRIPT_PATH##*/}"
PODMAN_NETWORK="net-${PROJECT_NAME}"

podman network create --ignore "${PODMAN_NETWORK}"

podman run \
  -td \
  --replace \
  --restart unless-stopped \
  --network "${PODMAN_NETWORK}" \
  -p "${CTR_PORT}":8501 \
  --name "${PROJECT_NAME}" \
  "${PROJECT_NAME}":latest

podman run \
  -td \
  --replace \
  --restart on-failure \
  --network "${PODMAN_NETWORK}" \
  --name "selenium" \
  "${PROJECT_NAME}"-test:latest

podman run \
  -td \
  --replace \
  --restart unless-stopped \
  --network "${PODMAN_NETWORK}" \
  --name "grid" \
  "docker.io/selenium/standalone-chrome:latest"
