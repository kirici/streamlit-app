#!/bin/bash
set -eu

# Global vars
SCRIPT_PATH="$(dirname "$(realpath "$0")")"
PROJECT_NAME="${SCRIPT_PATH##*/}"
PROJECT_NAME_TEST="${PROJECT_NAME}"-test

podman tag "${PROJECT_NAME}":latest "${PROJECT_NAME}":previous || true
podman build \
  --label project="${PROJECT_NAME}" \
  -t "${PROJECT_NAME}":latest \
  -f "${SCRIPT_PATH}"/Containerfile \
  "${SCRIPT_PATH}"
podman rmi -f "${PROJECT_NAME}":previous


podman tag "${PROJECT_NAME_TEST}":latest "${PROJECT_NAME_TEST}":previous || true
podman build \
  --label project="${PROJECT_NAME}" \
  -t "${PROJECT_NAME_TEST}":latest \
  -f "${SCRIPT_PATH}"/Containerfile.test \
  "${SCRIPT_PATH}"
podman rmi -f "${PROJECT_NAME_TEST}":previous
